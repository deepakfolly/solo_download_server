package testsocketserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;


public class socketserver {
	
	private static final int CHUNKSIZE = 2048; // default 
	private static final int MAX_FILE_SIZE = 600 * 1024; // 600 kb

	public static void main(String[] args) throws Exception {
		ServerSocket inSocket = new ServerSocket(9443);
		System.out.println("made socket");

		//File jfile = new File("C:\\Users\\Deepak Folly\\working\\magstripe\\appdev450\\EMERGE\\out\\AP.BIN");
		File jfile = new File ("/home/ubuntu/AP_S.BIN");

		if (!jfile.exists() || !jfile.isFile()) {
			System.out.println("Not a file");
		} else
			System.out.println("Is a file");

		while (true) {
			Socket sSocket = inSocket.accept();
			new Thread(new SocketHandler(jfile, sSocket)).start();
		}
	}



	static byte[] toBytes(int i) {
		byte[] result = new byte[4];

		result[0] = (byte) (i >> 24);
		result[1] = (byte) (i >> 16);
		result[2] = (byte) (i >> 8);
		result[3] = (byte) (i /* >> 0 */);

		return result;
	}
	
	private static void GetCurrentDateTime() {

	    //DateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
	    DateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
	    //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	  //day month year hour min sec
	        Date date = new Date();
	        System.out.println(sdf.format(date));
	        String datetimestring = sdf.format(date);
	        System.out.print(datetimestring);
	        
	//        Calendar cal = Calendar.getInstance();
	 //       System.out.println(sdf.format(cal.getTime()));

//	        LocalDateTime now = LocalDateTime.now();
//	        System.out.println(dtf.format(now));
//
//	        LocalDate localDate = LocalDate.now();
//	        System.out.println(DateTimeFormatter.ofPattern("yy/MM/dd").format(localDate));

	}
	

	private static class SocketHandler implements Runnable {

		private File jfile;
		private Socket sSocket;
		
		public SocketHandler(File jFile, Socket socket) {
			this.jfile = jFile;
			this.sSocket = socket;
		}
		
		@Override
		public void run()  {

			try {
				
				int chunkSize = CHUNKSIZE;
			
				sSocket.setSoTimeout(10000);
				
				String clientSentence = null;
				String logDeviceDetail = null;
				System.out.println("accepted socket");
				InputStreamReader 	isr = new InputStreamReader(sSocket.getInputStream());
				BufferedReader 		fromclient = new BufferedReader(isr);
				DataOutputStream 	toClient = new DataOutputStream(sSocket.getOutputStream());
				
				clientSentence = fromclient.readLine(); //{"type":"GETUPD"} STEP 1[C2S]
				System.out.println(clientSentence);
				logDeviceDetail =  clientSentence;
				
				// lets the download size 
			  	JSONObject json = new JSONObject(clientSentence.substring(2));
		        String PACKETSIZE = json.getString("PACKETSIZE");
		        int intPacketSize = Integer.parseInt(PACKETSIZE);
		        System.out.println("PACKETSIZE " + PACKETSIZE + " the converted int = " + intPacketSize);
		        chunkSize = intPacketSize;
				
				//Initialize all of the variables to spaces.
				byte[] padding = new byte[chunkSize];
				
				int filesize = (int) jfile.length();
				int byteposition = 0;
				int bytestowrite = chunkSize;
				System.out.println(filesize);
				
				/*  step 2 [S2C] */
				toClient.writeShort(4); // length of the length of payload
				toClient.flush();
				toClient.writeInt(filesize); // length of payload
				toClient.flush();
				
				
				clientSentence = fromclient.readLine(); //Step 3 [C2S]
				System.out.println(clientSentence);
				
				
				if (filesize > (640 * 1024)) {
					System.out.println("filesize too large ");
				}
				DataInputStream fis = new DataInputStream(new FileInputStream(jfile));
				byte[] buffer = new byte[chunkSize];
				{
					while (fis.read(buffer) != -1)// reads CHUNKSIZE at a time
					{
						if (byteposition + chunkSize > filesize) {
							bytestowrite = filesize - byteposition;
						} else {
							bytestowrite = chunkSize;
						}
						System.out.println(
								"before writing bytestowrite = " + bytestowrite + " byteposition " + byteposition);
						
						/* step 4 [C2S]	 */
						
						toClient.writeShort(bytestowrite); //len of chunksize pkt 
						toClient.write(buffer, 0, bytestowrite);// chunksize pkt
			
//						String bufferHex = DatatypeConverter.printHexBinary(buffer);  // for debug only 
//						System.out.println(bufferHex);								// for debug only	
//												
						/*********************************************************************************/
						if (bytestowrite < chunkSize)
						{
							System.out.println("write padding to scoket size = "  + (chunkSize-bytestowrite));
							toClient.write(padding, 0, chunkSize-bytestowrite);// chunksize pkt
							break;
						}
						/****************************************************************/
						System.out.println("after writing, waiting for ack");
						/* step 5 [c2S]  */
						clientSentence = fromclient.readLine();
						if (clientSentence.contains("ACK"))
						System.out.println("received ack from client = " + clientSentence);
						byteposition = byteposition + bytestowrite;
						System.out.println("looping byteposition " + byteposition);
					}
	

			}
			//System.out.println("exiting loop");
			System.out.println("Downlaod Complete for" + logDeviceDetail);

		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
		
	}
	
}



